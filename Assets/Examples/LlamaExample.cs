using LlamaWrapper;
using UnityEngine;

public class LlamaExample : MonoBehaviour
{
    [SerializeField]
#if UNITY_STANDALONE_WIN
    public string model = "D:/Documents/cmake/llama.cpp/models/llama-1B/Llama-1b-ggml-q4.bin";
#elif UNITY_STANDALONE_OSX
    public string model = "/Volumes/MACBOOK/macbook/Documents/cmake/llama.cpp/models/llama-1B/Llama-1b-ggml-q4.bin";
#else
    public string model = "";
#endif

    // Start is called before the first frame update
    void Start()
    {
        // Initialize llama
        Llama.Initialize(model);

        // Run llama
        Llama.Run("你好");
    }

    // Update is called once per frame
    void Update()
    {
        if (LlamaEventHandler.TextFinished())
        {
            string text = LlamaEventHandler.PopText();
            Debug.Log(text);
        }
    }

    void OnDestroy()
    {
        Llama.Destroy();
    }
}
