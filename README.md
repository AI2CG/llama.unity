# llama.unity

开源大语言模型llama的unity插件

### 系统

1. Windows
2. MacOS
3. ...

### 使用

unity version: 2020.3.48f1c1

### 模型

llama大语言模型：[https://huggingface.co/soulteary/Chinese-Llama-2-7b-ggml-q4](https://huggingface.co/soulteary/Chinese-Llama-2-7b-ggml-q4)

### 参考

1. llama.cpp：[https://github.com/ggerganov/llama.cpp](https://github.com/ggerganov/llama.cpp)
2. FreeAvatar：[https://gitee.com/AI2CG/FreeAvatar](https://gitee.com/AI2CG/FreeAvatar)

### 捐助

- 如果您觉得我们的开源软件对你有所帮助，请访问以下商店地址打赏我们一杯咖啡。
- Unity Asset Store搜索: [llama4unity](https://assetstore.unity.com/packages/tools/generative-ai/llama4unity-265968)
